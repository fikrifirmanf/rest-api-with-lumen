<?php

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function () {
    return Str::random(32);
});
$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');

    $router->get('users/{id}', 'UserController@singleUser');
    // Matches "/api/profile
    $router->get('profile', 'UserController@profile');
    $router->get('users', 'UserController@allUsers');
    $router->get('products',  ['uses' => 'ProductsController@showAllProducts']);

    $router->get('products/{id_products}', ['uses' => 'ProductsController@showOneProducts']);

    $router->post('products', ['uses' => 'ProductsController@create']);

    $router->delete('products/{id_products}', ['uses' => 'ProductsController@delete']);

    $router->put('products/{id_products}', ['uses' => 'ProductsController@update']);
});
