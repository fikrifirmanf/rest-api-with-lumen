<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function showAllProducts()
    {
        return response()->json(Products::paginate(10));
    }

    public function showOneProducts($id_products)
    {
        return response()->json(Products::find($id_products));
    }

    public function create(Request $request)
    {
        $products = Products::create($request->all());

        return response()->json($products, 201);
    }

    public function update($id_products, Request $request)
    {
        $products = Products::findOrFail($id_products);
        $products->update($request->all());

        return response()->json($products, 200);
    }

    public function delete($id_products)
    {
        Products::findOrFail($id_products)->delete();
        return response('Deleted Successfully', 200);
    }
}
